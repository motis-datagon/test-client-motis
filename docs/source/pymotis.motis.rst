pymotis.motis package
=====================

.. automodule:: pymotis.motis
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pymotis.motis.routing
