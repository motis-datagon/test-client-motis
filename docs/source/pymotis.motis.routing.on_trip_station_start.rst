pymotis.motis.routing.on\_trip\_station\_start module
=====================================================

.. automodule:: pymotis.motis.routing.on_trip_station_start
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
