"""
Test to get things working with the transitous endpoint
"""
from datetime import datetime, timedelta
import json
import time
from pymotis import api
from pymotis.model.options import TripRequestOptions


def test_journey_request():
    """
    Sends an example Journey Request to the transitous endpoint
    """
    client = api.get_client("transitous")
    answer = client.request_connection(start_id="de-DELFI_de:08111:2613",
                                              target_id="de-DELFI_de:08111:6169",
                                              departure_time=int(time.time()))
    answer_dict = json.loads(answer)
    assert len(answer_dict["itineraries"]) > 0


def test_get_trip():
    """
    Sends the same example request, but with the newly created function get_trips
    """
    client = api.get_client("transitous")
    tomorrow = int((datetime.now() + timedelta(days=1)).replace(hour=8, minute=0, second=0, microsecond=0).timestamp())
    trip_options = TripRequestOptions({"departure_time": tomorrow, "origin_id": "de-DELFI_de:05913:131:91:16",
                                       "destination_id": "de-DELFI_de:06439:6101:2:6",
                                       "start_type": "OnTripStationStart", "destination_type": "InputStation"})
    answer = client.get_trips(trip_options)
    answer_dict = json.loads(answer)
    assert len(answer_dict["itineraries"]) >= 5

    trip_options = TripRequestOptions({"departure_time": tomorrow, "origin_position": (47.839134, 7.715423),
                                       "destination_position": (47.919529, 7.69927699),
                                       "start_type": "IntermodalPreTripStart", "destination_type": "InputPosition"})
    answer = client.get_trips(trip_options)
    answer_dict = json.loads(answer)
    assert len(answer_dict["itineraries"]) >= 5


def test_write_request_file():
    """
    Writes an example Journey Request to a file
    """
    client = api.get_client("transitous")
    client.write_request_to_file(start_id="de-DELFI_de:08111:2613", target_id="de-DELFI_de:08111:6169",
                                 departure_time=int(time.time()), request_file_path="queries.txt")
