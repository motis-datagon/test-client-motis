"""
This module is meant to be used to connect to a motis-server-instance
"""
import importlib.resources as pkg_resources
import json
from datetime import datetime

from deprecated import deprecated

from pymotis import configurations
from pymotis.handler.motis_trip_handler import MOTISTripHandler
from pymotis.model import options


def get_client(config_name: str):
    """
    Offers a handy way to get a MOTIS-Client instance

    :param config_name: name of the config to load
    :return: a MOTIS-Client instance
    """
    if config_name.endswith(".json"):
        with open(config_name, encoding="utf-8") as config_file:
            configuration = json.load(config_file)
    else:
        config_filename = config_name + "-motis.json"
        config_file = pkg_resources.files(configurations).joinpath(config_filename).read_text()
        configuration = json.loads(config_file)
    url = configuration["options"]["endpoint"]
    motis_version = configuration["options"]["motisVersion"]
    client_options = options.ClientOptions(url, motis_version)
    return MotisClient(client_options)


class MotisClient:
    """
    Motis client for communicating with a Motis-API
    """

    def __init__(self, client_options: options.ClientOptions):
        """
        Constructor of the client

        :param client_options: configuration of a given client
        """
        self.__url: str = client_options.get_url()
        self.__motis_version = client_options.get_motis_version()
        self.journey_handler = MOTISTripHandler(client_options.get_url(), client_options.get_motis_version())

    @deprecated(reason="Use get_trips instead, as this function is limited to stop-to-stop routing")
    def request_connection(self, start_id: str, target_id: str, departure_time: int):
        """
        Returns the available trips based on specified options

        :return: the available trips
        """
        trip_options = options.TripRequestOptions({"departure_time": departure_time, "origin_id": start_id,
                                                   "destination_id": target_id, "start_type": "OnTripStationStart",
                                                   "destination_type": "InputStation"})
        response = self.journey_handler.get_trips(trip_options)
        return json.dumps(response.json(), indent=4)

    def get_trips(self, trip_options: options.TripRequestOptions):
        """
        Returns the available trips based on specified options

        :param trip_options: the options for which the trip is requested
        :return: the available trips
        """
        response = self.journey_handler.get_trips(trip_options)
        return json.dumps(response.json(), indent=4)

    def write_request_to_file(self, start_id: str, target_id: str, departure_time: int, request_file_path):
        """
        Writes a request to a file
        """
        trip_options = options.TripRequestOptions({"departure_time": departure_time, "origin_id": start_id,
                                                   "destination_id": target_id, "start_type": "OnTripStationStart",
                                                   "destination_type": "InputStation"})
        if self.__motis_version.startswith("0."):
            with open(request_file_path, "a+", encoding="utf-8") as request_file:
                request_file.write(self.journey_handler.get_trip_request(trip_options) + "\n")
        elif self.__motis_version.startswith("2."):
            with open(request_file_path, "a+", encoding="utf-8") as request_file:
                timestamp: datetime = datetime.fromtimestamp(trip_options.get_departure_time())
                iso_timestamp = timestamp.isoformat(timespec="milliseconds") + "Z"
                request_file.write(f"/api/v1/plan?time={iso_timestamp}&fromPlace={start_id}&toPlace={target_id}")
        else:
            raise ValueError("Unsupported Version")

    def get_url(self):
        """
        Returns the url of the client
        :return: the url of the client
        """
        return self.__url

    def get_motis_version(self):
        """
        Returns the motis_version of the server
        :return: the motis_version of the server
        """
        return self.__motis_version
