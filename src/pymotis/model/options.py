"""
This Module contains informal interfaces for parameter options
"""
from typing import Tuple


class ClientOptions:
    """
    Informal Interface for options that can be given to a client
    """

    def __init__(self, url, motis_version: str):
        self.__url = url
        self.__motis_version = motis_version

    def get_url(self):
        """
        Returns the url
        """
        return self.__url

    def get_motis_version(self):
        """
        Returns the motis version
        """
        return self.__motis_version


class TripRequestOptions:
    """
    Informal Interface for options that can be given to a TripHandler
    """

    def __init__(self, trip_request_options: {}):
        required_keys = ["departure_time", "start_type", "destination_type"]
        if not all(key in trip_request_options.keys() for key in required_keys):
            raise ValueError
        self.__departure_time: str = trip_request_options["departure_time"]
        self.__start_type: str = trip_request_options["start_type"]
        if self.__start_type == "OnTripStationStart":
            self.__origin_id: str = trip_request_options["origin_id"]
        elif self.__start_type == "IntermodalPreTripStart":
            self.__origin_position: Tuple[float, float] = trip_request_options["origin_position"]
            self.__origin_id = ""
        self.__destination_type: str = trip_request_options["destination_type"]
        if self.__destination_type == "InputStation":
            self.__destination_id: str = trip_request_options["destination_id"]
        elif self.__destination_type == "InputPosition":
            self.__destination_position: Tuple[float, float] = trip_request_options["destination_position"]
            self.__destination_id = ""

    def get_departure_time(self):
        """
        Returns the departure time of the origin
        :return: returns the departure time
        """
        return self.__departure_time

    def get_origin_id(self):
        """
        Returns the origin reference which can e.g. be a DHID
        :return: the origin reference of the origin
        """
        return self.__origin_id

    def has_origin_id(self):
        """
        Returns whether the origin id is set
        """
        return self.__origin_id != ""

    def get_destination_id(self):
        """
        Returns the destination id of the Trip
        :return: the destination id
        """
        return self.__destination_id

    def has_destination_id(self):
        """
        Returns whether the destination id is set
        """
        return self.__destination_id != ""

    def get_start_type(self):
        """
        Returns the start type of the trip

        :return: the start type of the trip
        """
        return self.__start_type

    def get_destination_type(self):
        """
        Returns the destination type of the trip

        :return: destination type of the trip
        """
        return self.__destination_type

    def get_destination_position(self):
        """
        Returns the destination position of the trip

        :return: destination position of the trip
        """
        return self.__destination_position

    def get_origin_position(self):
        """
        Returns the origin position of the trip

        :return: origin position of the trip
        """
        return self.__origin_position
