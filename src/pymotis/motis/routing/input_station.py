"""
Module for InputStation
"""


class InputStation:
    """
    An input station is a station from user input. If the user used the auto-completion function and the station ID is
    available, then the id field is used to resolve the station. If this is not the case
    (the user just entered a string), the name field is filled with a (possibly incomplete or misspelled) station name.
    In the latter case, MOTIS will use the first guess from the station auto-complete to resolve the station id.
    """

    def __init__(self, station_id: str, name: str):
        """
        Constructor of the InputStation

        Note: the parameter is named station_id instead of id, to avoid shadowing the built-in name id
        """
        self.__id: str = station_id
        self.__name: str = name

    def get_id(self):
        """
        Retrieve the id of the station

        :return: id of the station
        """
        return self.__id

    def get_name(self):
        """
        Retrieve the name of the station

        :return: name of the station
        """
        return self.__name

    def get_json_representation(self):
        """
        Returns the JSON representation as needed for a MOTIS request
        """
        return {"_type": "InputStation", "id": self.get_id(), "name": self.get_name()}
