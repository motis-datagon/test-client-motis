"""
Module for the OnTripStationStart
"""
from pymotis.motis.routing import input_station


class OnTripStationStart:
    """
    Class for the OnTripStationStart
    """

    def __init__(self, station: input_station, departure_time: int):
        """
        Constructor for the OnTripStationStart
        """
        self.__station: input_station = station
        self.__departure_time: int = departure_time

    def get_station(self):
        """
        Retrieve the station of the OnTripStationStart
        """
        return self.__station

    def get_departure_time(self):
        """
        Retrieve the departure time of the OnTripStationStart
        """
        return self.__departure_time
