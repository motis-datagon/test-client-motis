"""
Handler for MOTIS TripRequests
"""
from datetime import datetime
import json
from io import UnsupportedOperation

from pymotis.model.options import TripRequestOptions
from pymotis.motis.routing.input_station import InputStation
from pymotis.request import request


class MOTISTripHandler:
    """
    Handler for MOTIS TripRequest
    """

    def __init__(self, url: str, motis_version: str):
        self.__url = url
        self.__motis_version = motis_version

    def get_trip_request(self, trip_options: TripRequestOptions):
        """
        Creates a TripRequest according to the given configuration

        :param trip_options: options on which journeys should get retrieved
        :return: trip request build according to the options
        """
        if self.__motis_version.startswith("0."):
            start_type = trip_options.get_start_type()
            destination_type = trip_options.get_destination_type()
            if start_type == "OnTripStationStart":
                start = {"_type": start_type.replace("OnTrip", "Ontrip"),
                         "station": InputStation(trip_options.get_origin_id(), "").get_json_representation(),
                         "departure_time": trip_options.get_departure_time()}

            elif start_type == "IntermodalPreTripStart":
                start = {"_type": start_type.replace("PreTrip", "Pretrip"),
                         "position": {"lat": trip_options.get_origin_position()[0],
                                      "lng": trip_options.get_origin_position()[1]},
                         "interval": {"begin": trip_options.get_departure_time(),
                                      "end": trip_options.get_departure_time() + 3600},
                         "min_connection_count": 5,
                         "extend_interval_earlier": True,
                         "extend_interval_later": True}
            else:
                raise ValueError("Unsupported start type")
            if destination_type == "InputStation":
                destination = InputStation(trip_options.get_destination_id(), "").get_json_representation()
            elif destination_type == "InputPosition":
                destination = {"_type": destination_type,
                               "lat": trip_options.get_destination_position()[0],
                               "lng": trip_options.get_destination_position()[1]}
            else:
                raise ValueError("Unsupported destination type")
            trip_request = {
                "_type": "IntermodalRoutingRequest",
                "start": start,
                "start_modes": [
                    {"mode": {"_type": "FootPPR", "search_options": {"profile": "default", "duration_limit": 900}}}
                ],
                "destination": destination,
                "destination_modes": [
                    {"mode": {"_type": "FootPPR", "search_options": {"profile": "default", "duration_limit": 900}}}
                ],
                "search_type": "Default",
                "search_dir": "Forward",
                "router": ""
            }
            return json.dumps(trip_request)
        if self.__motis_version.startswith("2."):
            url = self.__url + "api/v1/plan?"
            timestamp: datetime = datetime.fromtimestamp(trip_options.get_departure_time())
            iso_timestamp = timestamp.isoformat(timespec="milliseconds") + "Z"
            url += f"time={iso_timestamp}&"
            if trip_options.has_origin_id():
                url += f"fromPlace={trip_options.get_origin_id()}&"
            else:
                url += f"fromPlace={trip_options.get_origin_position()[0]},{trip_options.get_origin_position()[1]},0&"
            if trip_options.has_destination_id():
                url += f"toPlace={trip_options.get_destination_id()}"
            else:
                url += (f"toPlace={trip_options.get_destination_position()[0]},"
                        f"{trip_options.get_destination_position()[1]},0&")
            url += ("&arriveBy=false&timetableView=true&pedestrianProfile=FOOT&preTransitModes=WALK&"
                    "postTransitModes=WALK&directModes=WALK")
            return url
        raise ValueError("Unsupported version")


    def get_trips(self, trip_options: TripRequestOptions):
        """
        Return the journeys according to the given configuration

        :param trip_options: options on which journeys should get retrieved
        :return: trips according to the given options
        """
        if self.__motis_version.startswith("0."):
            request_body = self.get_trip_request(trip_options)
            return request(url=self.get_url() + "intermodal", request_body=request_body)
        if self.__motis_version.startswith("2."):
            url = self.get_trip_request(trip_options)
            return request(url=url)
        raise UnsupportedOperation("Unsupported version")

    def get_url(self):
        """
        Returns the URL of the handler
        """
        return self.__url

    def get_motis_version(self):
        """
        Returns the MOTIS version of the handler
        """
        return self.__motis_version
