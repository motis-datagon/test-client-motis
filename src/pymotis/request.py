"""
module to deal with requests and responses
"""
from typing import Optional

import requests


def request(url: str, request_body: Optional[str] = None):
    """
    Sends a Request to a specified API

    :param url: url where to send the request to
    :param request_body: body for the request
    :return: the answer of the request
    """
    if request_body:
        return requests.post(url=url, data=request_body, timeout=10000)
    return requests.get(url=url, timeout=10000)
