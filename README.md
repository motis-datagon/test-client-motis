# Test-Client Motis
This project is a library to communicate with a motis-server

## Getting Started

1. Clone this repository
2. Set up the development environment: `pip install -e ".[dev]"`

### Prerequisites

You need a recent python version installed. Development is done with Python 3.10, no other python versions are tested 
yet.
Use the following command in a terminal to check your python version:
```shell
python --version
```

### Installing

1. Clone this repository
2. Install the module itself: `pip install -e .`

## Running the tests

Tests are placed in the `tests` directory. To execute them install test dependencies, install the repository locally and
run them with pytest:
```shell
pip install -e ".[tests]"
pytest
```

### And coding style tests
This projects uses pylint to check for coding style. Therefore, install pylint and execute it:

```shell
pip install -e ".[lints]"
pylint src --max-line-length=120
```

## Built With

* [requests](https://github.com/psf/requests) - a simple, yet elegant, HTTP library

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull 
requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the 
[tags on this repository](https://gitlab.com/motis-datagon/test-client-motis/-/tags). 

## What is MOTIS?

[MOTIS](https://motis-project.de/) stands for "Multi Objective Travel Information System", has been developed in scope
of the research by the Technical University of Darmstadt. 
MOTIS offers a wide-range list of functionalities, including station / location search, realtime departures, and so on.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

