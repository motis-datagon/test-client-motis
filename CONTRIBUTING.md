## Contributing

First off, thank you for considering contributing to pymotis.

### Where do I go from here?

If you've noticed a bug or have a feature request, [make one][new issue]! It's
generally best if you get confirmation of your bug or approval for your feature
request this way before starting to code.

### Fork & create a branch

If this is something you think you can fix, then [fork this repo] and create
a branch with a descriptive name.

A good branch name would be (if you fixed a typo in the README.md):

```sh
git checkout -b fix-typo-in-readme
```

### Implement your fix or feature

At this point, you're ready to make your changes! Feel free to ask for help;
everyone is a beginner at first :)

### Get the style right

Your patch should follow the same conventions & pass the same code quality
checks as the rest of the project. `pylint` will give you feedback in
this regard. When you pushed your fix the continuos integration pipeline should
give you some hints.

### Make a Merge Request

At this point, you should switch back to your development branch and make sure it's
up-to-date with pymotis development branch:

```sh
git remote add upstream git@gitlab.com:motis-datagon/test-client-motis.git
git checkout development
git pull upstream development
```

Then update your feature branch from your local copy of development, and push it!

```sh
git checkout fix-typo-in-readme
git rebase development
git push --set-upstream origin fix-typo-in-readme
```

Finally, go to Gitlab and [make a Merge Request] :D

Gitlab CI will run our linter. We care about quality, so your MR won't be merged until 
all tests pass.

### Keeping your Merge Request updated

If a maintainer asks you to "rebase" your MR, they're saying that a lot of code
has changed, and that you need to update your branch, so it's easier to merge.

To learn more about rebasing in Git, there are a lot of [good][git rebasing]
[resources][interactive rebase] but here's the suggested workflow:

```sh
git checkout fix-typo-in-readme
git pull --rebase upstream development
git push --force-with-lease fix-typo-in-readme
```

### Merging an MR (maintainers only)

An MR can only be merged into development by a maintainer if:

* It is passing CI.
* It has been approved by at least one maintainer. If it was a maintainer who
  opened the MR, one extra approval is needed.
* It has no requested changes.
* It is up-to-date with current development.

Any maintainer is allowed to merge an MR if all of these conditions are
met.

[new issue]: https://gitlab.com/motis-datagon/test-client-motis/-/issues/new
[fork this repo]: https://gitlab.com/motis-datagon/test-client-motis/-/forks/new
[make a Merge Request]: https://gitlab.com/motis-datagon/test-client-motis/-/merge_requests/new
[git rebasing]: http://git-scm.com/book/en/Git-Branching-Rebasing
[interactive rebase]: https://help.github.com/en/github/using-git/about-git-rebase
